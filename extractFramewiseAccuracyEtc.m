function [ best_accuracies,best_sensitivities,best_precisions,best_Fscores,...
    best_accuracies_bygt,best_sensitivities_bygt,best_precisions_bygt,best_Fscores_bygt,...
    matching_gt_labeIdx_perframe,matching_found_labelIdx_perframe] = ...
    extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,gtLabels)
%EXTRACTFRAMEWISEACCURACYETC Extracts segmentation statistics;
%   load_seg_file = @(frame): a callback that returns the image encoding the found
%   segmentation of frame. 
%   load_gt_seg_file: the same for the ground truth seg files
%   frames_with_gt: list of frames to evaluate
%   gtLabels: list of ground truth labels, allowed to have duplicates;
%   duplicates (e.g. [1,2,2,2,3]) mean that the ground truth label 2 is
%   allowed to be matches multiple times (e.g. 3 times) when establishing
%   one-to-one correspondences  between the found labeling and the gt.
%   E.g if there is a region that is allowed to be oversegmented
%   arbitrarely

num_found_labels = 0;
for f = frames_with_gt
   
   seg = load_seg_file(f);
   num_found_labels = max(num_found_labels, max(seg(:)));
   %imshow([label2rgb(seg, 'jet', 'k','shuffle'),load_gt_seg_vis_file(f)]);
   %drawnow;
   %pause(0.05)
end
num_found_labels
num_gt_labels = length(gtLabels);
tic;

accuracies = zeros(num_found_labels, length(gtLabels), length(frames_with_gt));
sensitivities =  zeros(num_found_labels, length(gtLabels), length(frames_with_gt));
Fscores =  zeros(num_found_labels, length(gtLabels), length(frames_with_gt));
precisions =  zeros(num_found_labels, length(gtLabels), length(frames_with_gt));

best_accuracies = zeros(num_found_labels, length(frames_with_gt));
best_sensitivities =  zeros(num_found_labels,  length(frames_with_gt));
best_Fscores =  zeros(num_found_labels, length(frames_with_gt));
best_precisions =  zeros(num_found_labels, length(frames_with_gt));

best_accuracies_bygt = zeros(num_gt_labels, length(frames_with_gt));
best_sensitivities_bygt =  zeros(num_gt_labels,  length(frames_with_gt));
best_Fscores_bygt =  zeros(num_gt_labels, length(frames_with_gt));
best_precisions_bygt =  zeros(num_gt_labels, length(frames_with_gt));

matching_found_seg = {};
matching_found_gt = {};

%frame_idx = 1;
for frame_idx = 1:length(frames_with_gt)
    frame = frames_with_gt(frame_idx);
    seg = load_seg_file(frame);
    accuracy = zeros(num_found_labels, length(gtLabels));
    sensitivity  = zeros(num_found_labels, length(gtLabels));
    precision  = zeros(num_found_labels, length(gtLabels));
    Fscore  = zeros(num_found_labels, length(gtLabels));

    %compute the accuracy etc of all gt-found label pairs
    for l_found = 1:num_found_labels
        % figure;
        for l_gt_idx = 1:length(gtLabels)
            %display(sprintf('%d,%d', l_found, l_gt_idx));
             l_gt = gtLabels(l_gt_idx);
            gt_seg = load_gt_seg_file(frame,l_gt);
            truepositive = (gt_seg == 1 |gt_seg == 3)& seg == l_found;
            num_true_pos = sum(truepositive(:));

            truenegative = (gt_seg == 2|gt_seg == 3)& (seg ~= l_found & seg > 0);
            num_true_neg = sum(truenegative(:));

            falsepositive = (gt_seg == 2)& seg == l_found;
            num_false_pos = sum(falsepositive(:));

            falsenegative = (gt_seg == 1)& (seg ~= l_found & seg > 0);
            num_false_neg = sum(falsenegative(:));

            accuracy(l_found, l_gt_idx) = num_true_pos/(num_true_pos + num_false_pos + num_false_neg);
            sensitivity(l_found, l_gt_idx) = num_true_pos/(num_true_pos + num_false_neg);
            precision(l_found, l_gt_idx) = num_true_pos/(num_true_pos + num_false_pos);
            Fscore(l_found, l_gt_idx) = 2* sensitivity(l_found, l_gt_idx)*precision(l_found, l_gt_idx)/( sensitivity(l_found, l_gt_idx)+precision(l_found, l_gt_idx));
            if(num_true_pos == 0)
                accuracy(l_found, l_gt_idx) =0;
                sensitivity(l_found, l_gt_idx) = 0;
                precision(l_found, l_gt_idx) = 0;
                Fscore(l_found, l_gt_idx) = 0;
            end
            
        end
    end
    accuracies(:,:,frame_idx) = accuracy;
    precisions(:,:,frame_idx) = precision;
    Fscores(:,:,frame_idx) = Fscore;
    sensitivities(:,:,frame_idx) = sensitivity;
 
    %establish optimal one-to-one mapping between found labels and gt
    [a, matching_found_labelIdx, matching_gt_labeIdx] = bipartite_matching(accuracy);%Fscore);
    matching_found_seg = [matching_found_seg,{matching_found_labelIdx}];
    matching_found_gt = [matching_found_gt,{matching_gt_labeIdx}];
    display('ok...')
    
    %variant one: store as quality per found label
    for l_found = 1:num_found_labels
        %figure;
       if(any( matching_found_labelIdx == l_found))
           
            gt_label_idx = matching_gt_labeIdx(matching_found_labelIdx == l_found);
            gt_label = gtLabels(gt_label_idx);
            best_accuracies(l_found, frame_idx) = accuracy(l_found,gt_label_idx);
            best_sensitivities(l_found, frame_idx) =   sensitivity(l_found,gt_label_idx);
            best_Fscores(l_found, frame_idx) =   Fscore(l_found,gt_label_idx);
            best_precisions(l_found, frame_idx) =  precision(l_found,gt_label_idx);
           %imshow([label2rgb(seg == l_found,'jet','k'), label2rgb(load_gt_seg_file(frame,gt_label))]);
       else
           % imshow([label2rgb(seg == l_found,'jet','k'),label2rgb(zeros(size(seg)))]);
       end
    end

    %variant 2: store as quality per ground truth label
    for gt_label_idx = 1:num_gt_labels
        %figure;
       if(any( matching_gt_labeIdx == gt_label_idx))
            l_found = matching_found_labelIdx(matching_gt_labeIdx == gt_label_idx);
            best_accuracies_bygt(gt_label_idx, frame_idx) = accuracy(l_found,gt_label_idx);
            best_sensitivities_bygt(gt_label_idx, frame_idx) =   sensitivity(l_found,gt_label_idx);
            best_Fscores_bygt(gt_label_idx, frame_idx) =   Fscore(l_found,gt_label_idx);
            best_precisions_bygt(gt_label_idx, frame_idx) =  precision(l_found,gt_label_idx);
       else
           % imshow([label2rgb(seg == l_found,'jet','k'),label2rgb(zeros(size(seg)))]);
       end
    end
    %k = waitforbuttonpress();
    %close all;
    
    matching_gt_labeIdx_perframe{frame_idx} = matching_gt_labeIdx;
    matching_found_labelIdx_perframe{ frame_idx} = matching_found_labelIdx';
end
 toc
% accuracy
end

