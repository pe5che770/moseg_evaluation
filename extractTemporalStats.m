function [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
    extractTemporalStats(load_seg_file, load_gt_consistency_file,frames_with_gt )
%EXTRACTTEMPORALSTATS Summary of this function goes here
%   Detailed explanation goes here


num_found_labels = 0;
for f = frames_with_gt
   seg = load_seg_file(f);
   num_found_labels = max(num_found_labels, max(seg(:)));
end
num_found_labels

num_consistency_regions =0;
for frame_idx = 1:(length(frames_with_gt)-1)
    frame = frames_with_gt(frame_idx);
       
    gt_seg = load_gt_consistency_file(frame);
    num_consistency_regions = max(num_consistency_regions,max(gt_seg(:)));
end
num_consistency_regions

num_per_region= {};
num_per_region_next = {};
D_label = zeros(num_found_labels,length(frames_with_gt)-1);
areas_label = zeros(num_found_labels,length(frames_with_gt)-1);
num_temp_errors_per_label = zeros(num_found_labels,length(frames_with_gt)-1);
num_temp_errors_per_pixel = zeros(num_found_labels,length(frames_with_gt)-1);
%alloc
 for region = 1: num_consistency_regions
        num_per_region{region} = zeros(num_found_labels,length(frames_with_gt)-1);
        num_per_region_next{region}= zeros(num_found_labels,length(frames_with_gt)-1);
 end
 
 
 % various problems:
 %vanishing labels, large labels covering multiple objects whose distribution change
for frame_idx = 1:(length(frames_with_gt)-1)
    frame = frames_with_gt(frame_idx);
    nextframe = frames_with_gt(frame_idx+1);
    
    gt_seg = load_gt_consistency_file(frame);
    gt_seg_next = load_gt_consistency_file(nextframe);
    seg = load_seg_file(frame);
    seg_next = load_seg_file(nextframe);
    total_sz = sum(seg(:)>0);
    
    for region = 1: num_consistency_regions
        bla = seg(gt_seg == region);
        bla_next = seg_next(gt_seg_next == region);
        for l = 1:num_found_labels
              num_per_region{region}(l,frame_idx) = sum(bla(:) == l);
              num_per_region_next{region}(l,frame_idx) = sum(bla_next(:) == l);
              areas_label(l,frame_idx) = sum(seg(:)==l);
        end
        region_sz = sum(bla>0);
        region_next_sz = sum(bla_next>0);
        if true%(region_sz < 1.5* region_next_sz && 1.5*region_sz > region_next_sz)
            num_per_region{region}(:,frame_idx) =  num_per_region{region}(:, frame_idx) /region_sz ;
            num_per_region_next{region}(:,frame_idx) =  num_per_region_next{region}(:, frame_idx) /region_next_sz ;
            D_label(:,frame_idx) =  D_label(:,frame_idx) + double(region_sz)*max(num_per_region{region}(:,frame_idx) -num_per_region_next{region}(:,frame_idx) ,0);
        end
        
    end
    num_temp_errors_per_label(:, frame_idx) = D_label(:,frame_idx)./ (1+areas_label(:,frame_idx));%areas are in pixel, +1 does not hurt.
    num_temp_errors_per_pixel(:, frame_idx) = D_label(:,frame_idx)/total_sz;
end

end

