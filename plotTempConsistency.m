function plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

figure('name',method_name);
num_found_labels = size(num_temp_errors_per_label,1);
for l = 1:num_found_labels
    subplot(1,2,1)
    plot(frames_with_gt(1:end-1)+0.5, cumsum(num_temp_errors_per_label(l,:)),':')
      
    hold on;
    subplot(1,2,2)
    plot(frames_with_gt(1:end-1)+0.5,cumsum(num_temp_errors_per_pixel(l,:)),':')
    hold on;
end
subplot(1,2,1)
plot(frames_with_gt(1:end-1)+0.5, mean(cumsum(num_temp_errors_per_label,2),1),'k')

hold on;
subplot(1,2,2)
plot(frames_with_gt(1:end-1)+0.5,mean(cumsum(num_temp_errors_per_pixel,2),1),'k')
hold on;

%
subplot(1,2,1)
temp_err_per_label =  mean(sum(num_temp_errors_per_label,2));
title(sprintf('Cumulative temporal Error (per label): mean %3.2f', temp_err_per_label))
subplot(1,2,2)
temp_err_per_pixel =  mean(sum(num_temp_errors_per_pixel,2));
title(sprintf('Cumulative temporal Error (per pixel): mean %3.5f', temp_err_per_pixel))