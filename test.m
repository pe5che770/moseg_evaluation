
%alternative: imseggeodesic



%%

col = imread('col00651.png');
depth = imread('depth00651.png');
depth = double(depth) * 0.0002;

%%
figure;
%[LINEOBJ,XS,YS] = freehanddraw(gca);


masked_imgx = col(:,:,1);
masked_imgy = col(:,:,2);
masked_imgz = col(:,:,3);
masked(:,:,1) = masked_imgx;
masked(:,:,2) = masked_imgy;
masked(:,:,3) = masked_imgz;

imshow(masked);
h = imfreehand(gca);
setClosed(h, false)
mask1 = createMask(h);
%figure; imshow(mask)

masked_imgx(mask1) = 0;
masked_imgy(mask1) = 255;
masked_imgz(mask1) = 0;
masked(:,:,1) = masked_imgx;
masked(:,:,2) = masked_imgy;
masked(:,:,3) = masked_imgz;


imshow(masked)
h = imfreehand(gca);
setClosed(h, false)
mask2 = createMask(h);



masked_imgx(mask2) = 255;
masked_imgy(mask2) = 0;
masked_imgz(mask2) = 0;
masked(:,:,1) = masked_imgx;
masked(:,:,2) = masked_imgy;
masked(:,:,3) = masked_imgz;


imshow(masked)
title('neutral region')
h = imfreehand(gca);
setClosed(h, false)
mask3 = createMask(h);

masked_imgx(mask3) = 0;
masked_imgy(mask3) = 0;
masked_imgz(mask3) = 255;
masked(:,:,1) = masked_imgx;
masked(:,:,2) = masked_imgy;
masked(:,:,3) = masked_imgz;
%%
figure;
imshow(masked)



d2 = regionfill(depth,(depth == 0));

[dx,dy] = gradient(d2);

[dxx,dxy] = gradient(dx);
[dyx,dyy] = gradient(dy);
second_deriv = dxx.^2 + dyy.^2;
%dx = imfilter(d2, [-1 1], 'same');
%dy = imfilter(d2', [-1 1], 'same')';
figure;
gradMagnitude = (dx.^2 + dy.^2);
smoothness_depthgrad = exp(-gradMagnitude/mean(gradMagnitude(:)));
subplot(1,4,1);
imshow(smoothness_depthgrad/max(smoothness_depthgrad(:)))
%
intensity = mean(col,3);
intensity = medfilt2(intensity,[5,5]);
subplot(1,4,2);imshow(intensity/max(intensity(:)));
[dx,dy] = gradient(intensity);
col_smoothness = dx.^2 + dy.^2;
col_smoothness = log(1+ col_smoothness);
col_smoothness = 1-col_smoothness/max(col_smoothness(:));%exp(-bla/mean(bla(:)));
%bla = log(1+100*second_deriv/mean(second_deriv(:)));
subplot(1,4,3);
imshow(col_smoothness/max(col_smoothness(:)))
subplot(1,4,4);
bla = min(col_smoothness, smoothness_depthgrad);
imshow(bla/max(bla(:)))

%%
alpha_smoothnes = 0.001;


clear dataterm
%numLabels = 2;
%height = 30;width = 60;
%dataterm(:,1:(width/2),1) = 0.52;
%dataterm(:,:,2) = 1-dataterm(:,:,1):

numLabels = 3;
height = size(mask1,1);width = size(mask1,2);
dataterm(:,:,1) = -log(0.95*double(mask1) + 0.05);
dataterm(:,:,2) = -log(0.95*double(mask2) + 0.05) ;
dataterm(:,:,3) = -log(0.95*double(mask3) + 0.05);


figure; hh = surf(dataterm(:,:,1)); set(hh, 'edgecolor','none')
figure; hh = surf(dataterm(:,:,2));set(hh, 'edgecolor','none')


smoothness = ones(numLabels) - eye(numLabels,numLabels); %spatially unvarrying.

%vertical_smoothness = alpha_smoothnes *ones(height,width);%(i,j)->(i+1,j)
%horizontal_smoothness = alpha_smoothnes *ones(height,width);%(i,j)->(i+1,j)
vertical_smoothness = smoothness_depthgrad;
horizontal_smoothness = smoothness_depthgrad;

%[gridx, gridy] = meshgrid(1:width,1:height);
%horizontal_smoothness((gridx-width/2).^2 + (gridy-height/2).^2 < 55) = 10;
%vertical_smoothness = horizontal_smoothness;
figure; hh = surf(horizontal_smoothness*400);set(hh, 'edgecolor','none'); axis equal;


%%

[gch] = GraphCut('open', dataterm, smoothness, vertical_smoothness, horizontal_smoothness);

%[gch] = GraphCut('set', gch, labels) %initial guess?

iter = 10;
[gch, labels] = GraphCut('swap', gch, iter);
labels = labels +1;
labels(depth == 0) = 0;

Lrgb = label2rgb(labels, 'jet', 'w', 'shuffle');

figure;
imshow(Lrgb)


%[gch labels] = GraphCut('get', gch);

[gch] = GraphCut('close', gch);
clear gch;


%% writing out

constraints = handles.masks;

u8_grayscale = uint8(zeros(size(constraints{1})));
for i = 1: length(constraints)
    u8_grayscale(constraints{i}) = i;
end
imwrite(u8_grayscale, 'constraints.png') ;


%% reading in

%%
labeling = handles.output_labeling;
u8_grayscale = uint8(zeros(size(labeling)));
for i = 1: max(labeling(:))
    u8_grayscale(labeling == i) = i;
end
imwrite(u8_grayscale, 'labeling.png') ;



%% 
%%
%dream of warping...
f = frames{1};
f_target = frames{2};
figure;

seg = imread('constraints.png');
dimx = size(seg,1);
dimy =size(seg,2);
[gridx, gridy] = meshgrid(1:dimy,1:dimx);

%flow method...
in.method =  @Flow1;       %Locally regularized and vectorized method
% in.method =  @FlowHS;      %Horn and Schunk, variational (global regularization)
in.MaxIterations = 10;
in.vidRes  = size(frameGray1);%[128 128];   %video resolution, does not affect file-input(avi or saved-folder)
in.flowRes = round(size(frameGray1));%[128 128];     %flow resolution
in.tIntegration =0;
[u,v] = in.method(in, frameGray1, frameGray2);

%%
while f < f_target
    col_file = sprintf(color_filename_mask, f);
    col_file2 = sprintf(color_filename_mask, f+1);
    col = imread(col_file);
    frameGray1 = rgb2gray(col);   
    col2 = imread(col_file2);
    frameGray2 = rgb2gray(col2);  
    
    %simple, hacked, dump warping...
    [u,v] = in.method(in, frameGray1, frameGray2);
    targetx = min(max(gridx+u, dimy),1);
    targety = min(max(gridy+v, dimx),1);
    
    %refine this a bit!.
    indices_target =  targetx(:) + targety(:)*dimx;
    origin_target = gridx(:) + gridy(:)*dimx;
    warped_seg = seg;
    warped_seg(indices_target) = seg(origin_target);
    
    LRGB = label2rgb(seg,'jet','w','shuffle');
    imshow(LRGB);
    %imshow(frameGray1);
    
    pause(0.1)
    f=f+1;
end

%%
% in.method =  @FlowLK;      %Lucas and Kanade 
in.method =  @Flow1;       %Locally regularized and vectorized method
%in.method =  @FlowHS;      %Horn and Schunk, variational (global regularization)
in.MaxIterations = 10;
in.vidRes  = size(frameGray1);%[128 128];   %video resolution, does not affect file-input(avi or saved-folder)
in.flowRes = round(size(frameGray1));%[128 128];     %flow resolution
in.tIntegration =0;
[u,v] = in.method(in, frameGray2,frameGray1);

figure;
imshow(frameGray1)
hold on;
quiver(u*10,v*10,0)

dimx = size(frameGray1,2);
dimy =size(frameGray1,1);
%[coordy,coordx] = meshgrid(1:dimy,1:dimx);
%ubar = u'; vbar = v';
[coordx,coordy] = meshgrid(1:dimx,1:dimy);
plot([coordx(139,136),coordx(139,136) + u(139,136)*9],[coordy(139,136),coordy(139,136) + v(139,136)*9])

%figure;surf(coordx, coordy, ubar.^2 + vbar.^2)
%%
u = 30*u;
v = 30*v;
close all;
%warping...
targetx = max(min(coordx+u, dimx),1);
targety = max(min(coordy+v, dimy),1);
%
indices_target =  (round(targetx(:))-1)*dimy + (round(targety(:))-1)+1;
origin_target = (coordx(:)-1)*dimy + (coordy(:)-1) +1;
bla = frameGray2;
figure; imshow(bla); title('target');
bla = zeros(size(frameGray2));
bla(indices_target) = frameGray1(origin_target);
figure; imshow(bla/255); title('warped');
figure; imshow(frameGray1); title('original');

%%
opticFlow = opticalFlowLK('NoiseThreshold', 0.009);
% Compute optical flow
flow = estimateFlow(opticFlow, frameGray1); 
imshow(frameRGB)
hold on
plot(flow,'DecimationFactor',[5 5],'ScaleFactor',10)
hold off
