function plotAccuraciesETC( best_accuracies,best_sensitivities,best_precisions,best_Fscores,...
    frames_with_gt,gtVisibleInFrame, method_name )
%VISUALIZEACCURACIESETC SCreates a figure to plot the extracted stats
%   frames_to_consider_byLabel a function returning array of gt frame idcs.

fmask = gtVisibleInFrame;

masked_mean = @(mask, mat, dim) sum(mask.*mat,dim)./sum(mask,dim);

num_found_labels = size(best_accuracies,1);
figure('name',method_name);
 subplot(2,4,1)
 plots_l = [];
 names = {};
 means = zeros(num_found_labels,1);
for l_found = 1:num_found_labels
    p = plot3(frames_with_gt(fmask(l_found,:)),best_accuracies(l_found,fmask(l_found,:)),l_found * ones(sum(fmask(l_found,:))),':');
    
    %plots_l =[plots_l,p];
    %names = [names;{num2str(l_found)}];
    hold on;
end
%legend(plots_l, names) 
view(0,90)
plot(frames_with_gt,masked_mean(fmask, best_accuracies,1),'k');%sum(fmask.*best_accuracies,1)./sum(fmask,1),'k');
title(sprintf('Accuracy: %1.3f',mean(masked_mean(fmask, best_accuracies,2))))%mean(best_accuracies(:))));
subplot(2,4,2)
for l_found = 1:num_found_labels
    plot(frames_with_gt(fmask(l_found,:)),best_sensitivities(l_found,fmask(l_found,:)),':');
    hold on;
end
plot(frames_with_gt,masked_mean(fmask, best_sensitivities,1),'k')%mean(best_sensitivities,1),'k');
title(sprintf('sensitivity: %1.3f',mean(masked_mean(fmask, best_sensitivities,2))))%mean(best_sensitivities(:))));
subplot(2,4,3)
for l_found = 1:num_found_labels
    plot(frames_with_gt(fmask(l_found,:)),best_precisions(l_found,fmask(l_found,:)),':');
    hold on;
end
plot(frames_with_gt,masked_mean(fmask, best_precisions,1),'k')%mean(best_precisions,1),'k');
title(sprintf('precision: %1.3f',mean(masked_mean(fmask, best_precisions,2))))%mean(best_precisions(:))));
subplot(2,4,4)
for l_found = 1:num_found_labels
    plot(frames_with_gt(fmask(l_found,:)),best_Fscores(l_found,fmask(l_found,:)),':');
    hold on;
end
plot(frames_with_gt,masked_mean(fmask, best_Fscores,1),'k')%mean(best_Fscores,1),'k');
title(sprintf('Fscore%1.3f',mean(masked_mean(fmask, best_Fscores,2))))%mean(best_Fscores(:))));

%
subplot(2,4,5)
bar((masked_mean(fmask,best_accuracies,2)));
hold on;
plot(mean(mean(best_accuracies))* ones(num_found_labels,1),'r');
title('Accuracies per label');
subplot(2,4,6)
bar((masked_mean(fmask,best_sensitivities,2)));
hold on;
plot(mean(mean(best_sensitivities))* ones(num_found_labels,1),'r');
title('sensitivity per label');
subplot(2,4,7)
 bar((masked_mean(fmask,best_precisions,2)));
 hold on;
plot(mean(mean(best_precisions))* ones(num_found_labels,1),'r');
title('precision per label');
subplot(2,4,8)
 bar((masked_mean(fmask,best_Fscores,2)));
 hold on;
plot(mean(mean(best_Fscores))* ones(num_found_labels,1),'r');
title('Fscore per label');
end

