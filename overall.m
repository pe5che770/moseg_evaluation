%% create nice figures.

%sintel
clear all;
data.name = 'ambush5';
data.gtLabels = 0:8;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:2;
data.nearlyRigidLabelsIdx = 3:9;
%data.frames_with_gt = 4:4:20; 
data.frames_with_gt = 8:4:20; 
data.gt_folder = 'sintel/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/sintel_run/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/sintel_full_7_2_2017_2000_200_20/result_png/%d.png', frame-1);
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/sintel/cast2eval/labeling_frame%d.png', frame-1);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/Sintel/vis_hrd_seg/seg_frm%d.png', frame-1);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),10));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),20));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% Sintel one per it plot:
name_frame_it = @(frame, it) sprintf(['G:/PHD/CRF/recompiled24_11/Sintel/correctedParam_mrg_MAX32Hyp_mnbMrgtrue_outltrue_hypcost1.005/',...'
    '8e29cd9e5850291bb95ceb_crf_hrd%d/seg_frm%d.png'], it, frame-1);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
vis_it0 = @(frame ) (flip_img(imread(name_frame_it(frame,0))));
vis_it1 = @(frame ) (flip_img(imread(name_frame_it(frame,1))));
vis_it2 = @(frame ) (flip_img(imread(name_frame_it(frame,2))));
vis_it3 = @(frame ) (flip_img(imread(name_frame_it(frame,3))));
vis_it4 = @(frame ) (flip_img(imread(name_frame_it(frame,4))));
vis_it5 = @(frame ) (flip_img(imread(name_frame_it(frame,5))));
vis_it6 = @(frame ) (flip_img(imread(name_frame_it(frame,6))));
vis_it7 = @(frame ) (flip_img(imread(name_frame_it(frame,7))));
vis_it8 = @(frame ) (flip_img(imread(name_frame_it(frame,8))));
vis_it9 = @(frame ) (flip_img(imread(name_frame_it(frame,9))));
data.name = 'ambush5_it';
data.who_to_vis = {vis_it0,vis_it1,vis_it2, vis_it3, vis_it4, vis_it5, vis_it6, vis_it7, vis_it8, vis_it9};
figure;
niceFig = createResultFigure(data,true, false);

%% one chair
clear all;
data.name = 'one_chair';
data.gt_folder = 'one_chair3/';
data.gtLabels = 0:11;%[0:9,9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4:12;
data.frames_with_gt = (700-2*24):24:(700+2*24); %(700-2*24):6:(700+2*24); 

data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/one_chair_run/labeling_frame%d.png', frame);
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/one_chair/cast2Eval/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/chair_full_7_2_2017_2000_200_20/result_png/%d.png', ...
    (frame - (700-2*24))/2+1);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/one_chair/vis_hrd_seg/seg_frm%d.png', (frame - (700-2*24))/2);

data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),8));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),10));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% noinit one chair
clear all;
data.name = 'noinit_one_chair';
data.gt_folder = 'one_chair3/';
data.gtLabels = 0:11;%[0:9,9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4:12;
data.frames_with_gt = (700-2*24):24:(700+2*24); %(700-2*24):6:(700+2*24); 

data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/one_chair_run/labeling_frame%d.png', frame);
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11_noninit/one_chair/_fullrun_2_11_genBySM_shft_MAX32Hyp_mnbMrgtrue_outltrue_stdhypcost1.005/8e29cd9e5850291bb95ceb_png4eval_9/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/chair_full_7_2_2017_2000_200_20/result_png/%d.png', ...
    (frame - (700-2*24))/2+1);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11_noninit/one_chair/_fullrun_2_11_genBySM_shft_MAX32Hyp_mnbMrgtrue_outltrue_stdhypcost1.005/8e29cd9e5850291bb95ceb_crf_hrd9/seg_frm%d.png', (frame - (700-2*24))/2);

data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),8));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),10));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% Sintel one per it plot:
name_frame_it = @(frame, it) sprintf(['G:/PHD/CRF/recompiled24_11/one_chair/'...
    'fullrun_2_11_genBySM_shft_MAX32Hyp_mnbMrgtrue_outltrue/8e29cd9e5850291bb95ceb_crf_hrd%d/seg_frm%d.png'], it, (frame - (700-2*24))/2);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
vis_it0 = @(frame ) (flip_img(imread(name_frame_it(frame,0))));
vis_it1 = @(frame ) (flip_img(imread(name_frame_it(frame,1))));
vis_it2 = @(frame ) (flip_img(imread(name_frame_it(frame,2))));
vis_it3 = @(frame ) (flip_img(imread(name_frame_it(frame,3))));
vis_it4 = @(frame ) (flip_img(imread(name_frame_it(frame,4))));
vis_it5 = @(frame ) (flip_img(imread(name_frame_it(frame,5))));
vis_it6 = @(frame ) (flip_img(imread(name_frame_it(frame,6))));
vis_it7 = @(frame ) (flip_img(imread(name_frame_it(frame,7))));
vis_it8 = @(frame ) (flip_img(imread(name_frame_it(frame,8))));
vis_it9 = @(frame ) (flip_img(imread(name_frame_it(frame,9))));
data.frames_with_gt = (700-2*24):12:(700+2*24); %(700-2*24):6:(700+2*24); 
data.name = 'one_chair_it';
data.who_to_vis = {vis_it0,vis_it1,vis_it2, vis_it3, vis_it4, vis_it5, vis_it6, vis_it7, vis_it8, vis_it9};
figure;
niceFig = createResultFigure(data,true, false);

%% two chairs
clear all;
data.name = 'two_chairs';
data.gtLabels = 0:2;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = [];
data.frames_with_gt = 10:10:50; 
data.gt_folder = 'my_two_chairs/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/my_two_chairs_run/labeling_frame%d.png', frame);
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/two_chairs/cast2eval/labeling_frame%d.png', frame-1);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/two_chairs_full_7_2_2017_2000_200_20/result_png/%d.png', frame);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/two_chairs/vis_hrd_seg/seg_frm%d.png', frame-1);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),8));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),10,4));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);


%% bonn chair

clear all;
data.name = 'bonn_chair';
data.gtLabels = 0:3;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4;
%data.frames_with_gt = (375-5*15):30:(375+5*15); 
data.frames_with_gt = (375-5*15):30:(375+5*15)-30; 
data.gt_folder = 'bonn_chair/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/bonn_chair_run/labeling_frame%d.png', ...
    (frame-5)*double(frame <400) + (frame-7)*double(frame>=400)); % dont ask.
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_chair/cast2eval/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/bonn_chair_full_7_2_2017_2000_200_20/result_png/%d.png', (frame-(375-3*30))/3);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_chair/vis_hrd_seg/seg_frm%d.png', (frame-(375-3*30))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),8));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),10));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% bonn_can_1

clear all;
data.name = 'bonn_can_1';
data.gtLabels = 0:5;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4:6;
data.frames_with_gt = 740:30:860;  
data.gt_folder = 'bonn_can/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/bonn_giesskanne_4iros/labeling_frame%d.png',...
    frame- 87 - 4*double(frame>760) -double(frame>790) -4*double(frame>820) -4*double(frame>850)); % dont ask.
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_can_4iros/cast2eval/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/bonn_can4iros_full_7_2_2017_2000_200_20/result_png/%d.png', (frame-(740))/3+1);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_can_4iros/vis_hrd_seg/seg_frm%d.png', (frame-(740))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),5));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),13));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% noini bonn_can_1

clear all;
folder = 'G:/PHD/CRF/recompiled24_11_noninit/bonn_can_4iros/runbonnCalib_2_11_MAX32Hyp_mnbMrgtrue_outltrue_crfSp4_stdhypcost1.005/';
my_seg = [folder,'8e29cd9e5850291bb95ceb_png4eval_9'];
my_seg_vis = [folder,'8e29cd9e5850291bb95ceb_crf_hrd9'];
data.name = 'noinit_bonn_can_1';
data.gtLabels = 0:5;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4:6;
data.frames_with_gt = 740:30:860;  
data.gt_folder = 'bonn_can/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/bonn_giesskanne_4iros/labeling_frame%d.png',...
    frame- 87 - 4*double(frame>760) -double(frame>790) -4*double(frame>820) -4*double(frame>850)); % dont ask.
data.found_seg_file_OURS = @(frame) sprintf([myseg,'/labeling_frame%d.png'], frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/bonn_can4iros_full_7_2_2017_2000_200_20/result_png/%d.png', (frame-(740))/3+1);
data.seg_vis_file_OURS = @(frame) sprintf([my_seg_vis,'/seg_frm%d.png'], (frame-(740))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),5));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),13));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);



%% bonn_can_2

clear all;
data.name = 'bonn_can_2';
data.gtLabels = 0:2;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:2;
data.nearlyRigidLabelsIdx = 3;
data.frames_with_gt =240:15:285;  
data.gt_folder = 'bonn_can_singlemo/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/bonn_giesskanne_simple/labeling_frame%d.png', ...
    (frame- 28) - 2*double(frame>=255)  - 5*double(frame>=280)); % dont ask.
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_can_simple_correctframe/cast2eval/labeling_frame%d.png', frame);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/bonn_can_simple_full_7_2_2017_2000_200_20/result_png/%d.png', (frame-(225))/3+1);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/bonn_can_simple_correctframe/vis_hrd_seg/seg_frm%d.png', (frame-(225))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),5));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),13));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% statue

clear all;
data.name = 'statue';
data.gtLabels = 0:4;%[0:9,9:11];%0:10;
data.rigidLabelIdx = 1:3;
data.nearlyRigidLabelsIdx = 4:5;
data.frames_with_gt =1900:42*2:2530;   
data.gt_folder = 'statue_fixed/';
data.gt_seg_file = @(frame, label) sprintf([data.gt_folder,'GT_label%d_frame%d.png'], label,frame);
data.gt_consistency_file = @(frame) sprintf([data.gt_folder,'OS_frame%d.png'], frame);
data.load_gt_seg_file  = @(frame, label) imread(data.gt_seg_file(frame,label));

flip_img = @(img)(img(:,end:-1:1,:));
data.found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/statue_run/labeling_frame%d.png', 1899+ (frame-1897)/3);
data.found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/cast2Eval/labeling_frame%d.png', (frame - (1897))/3);
data.found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/statue_full_7_2_2017_2000_200_20/result_png/%d.png', (frame - (1897))/3);
data.seg_vis_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/vis_hrd_seg/seg_frm%d.png', (frame-(1897))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
data.load_seg_vis_IROS = @(frame ) (nice_vis(imread(data.found_seg_file_IROS(frame)),5));
data.load_seg_vis_EG16 = @(frame ) (nice_vis(imread(data.found_seg_file_EG16(frame)),13));
data.load_seg_vis_ours = @(frame ) (flip_img(imread(data.seg_vis_file_OURS(frame))));

data.who_to_vis = {data.load_seg_vis_IROS,data.load_seg_vis_EG16 ,data.load_seg_vis_ours};
figure;
niceFig = createResultFigure(data,true);

%% statue
name_frame_it = @(frame, it) sprintf(['G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/'...
    'full_mrg_sigsp6MAX32Hyp_mnbMrgtrue_outltrue/8e29cd9e5850291bb95ceb_crf_hrd%d/seg_frm%d.png'], it, (frame-(1897))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
vis_it0 = @(frame ) (flip_img(imread(name_frame_it(frame,0))));
vis_it1 = @(frame ) (flip_img(imread(name_frame_it(frame,1))));
vis_it2 = @(frame ) (flip_img(imread(name_frame_it(frame,2))));
vis_it3 = @(frame ) (flip_img(imread(name_frame_it(frame,3))));
vis_it4 = @(frame ) (flip_img(imread(name_frame_it(frame,4))));
vis_it5 = @(frame ) (flip_img(imread(name_frame_it(frame,5))));
vis_it6 = @(frame ) (flip_img(imread(name_frame_it(frame,6))));
vis_it7 = @(frame ) (flip_img(imread(name_frame_it(frame,7))));
vis_it8 = @(frame ) (flip_img(imread(name_frame_it(frame,8))));
vis_it9 = @(frame ) (flip_img(imread(name_frame_it(frame,9))));
data.frames_with_gt =1900:42*2:2530;   
data.name = 'statue_it';
data.who_to_vis = {vis_it0,vis_it1,vis_it2, vis_it3, vis_it4, vis_it5, vis_it6, vis_it7, vis_it8, vis_it9};
figure;
niceFig = createResultFigure(data,true, false);


%% statue no init
flip_img = @(img)(img(:,end:-1:1,:));
name_frame_it = @(frame, it) sprintf(['G:/PHD/CRF/recompiled24_11_noninit/statue/'...
    'rere_full_mrg_sigsp6MAX32Hyp_mnbMrgtrue_outltrue_cost1.005001/8e29cd9e5850291bb95ceb_crf_hrd%d/seg_frm%d.png'], it, (frame-(1897))/3);
%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
vis_it0 = @(frame ) (flip_img(imread(name_frame_it(frame,0))));
vis_it1 = @(frame ) (flip_img(imread(name_frame_it(frame,1))));
vis_it2 = @(frame ) (flip_img(imread(name_frame_it(frame,2))));
vis_it3 = @(frame ) (flip_img(imread(name_frame_it(frame,3))));
vis_it4 = @(frame ) (flip_img(imread(name_frame_it(frame,4))));
vis_it5 = @(frame ) (flip_img(imread(name_frame_it(frame,5))));
vis_it6 = @(frame ) (flip_img(imread(name_frame_it(frame,6))));
vis_it7 = @(frame ) (flip_img(imread(name_frame_it(frame,7))));
vis_it8 = @(frame ) (flip_img(imread(name_frame_it(frame,8))));
vis_it9 = @(frame ) (flip_img(imread(name_frame_it(frame,9))));
data.frames_with_gt =1900:42*2:2530;   
data.name = 'noinit_statue_it';
data.who_to_vis = {vis_it0,vis_it1,vis_it2, vis_it3, vis_it4, vis_it5, vis_it6, vis_it7, vis_it8, vis_it9};
figure;
niceFig = createResultFigure(data,true, false);


%% visualization of ternary masks
 clear all;

 
 %% visualization of energy decrease
 figure;
 plot(0.5:0.5:5,[53,48,44.5,43.5,43,43,43,42.5,42.5,42])
 axis([0 6 0 60])

 
 %%
 its = 1:10;
 semilog = true;
 out_fig =  figure;
  names = {};
 two_chair_e = [181,156,111,106,107,106.5,107,107,106.5,107];two_chairs_numMo = [6,10,8,8,8,8,8,8,8,8]-5;
 %
 if semilog
    semilogy(its,two_chair_e.*1.005.^two_chairs_numMo);
    axis([0 15 10 500])
    out_name = 'paperFigs/energyDecay_log';
 else
 plot(its,two_chair_e.*1.005.^two_chairs_numMo);
 axis([0 11 10 500])
 out_name = 'paperFigs/energyDecay';
 end
 
 names = [names, {'two chairs'}];
 
 hold on;
 %
% figure;
 one_chair_e = [156,136,116,112.5,110,108.5,107,105.5,105.5,105.5]; one_chair_numMo = [6,9,9,12,12,13,13,13,13,13]-5;
 if semilog
    semilogy(its,one_chair_e.*1.005.^one_chair_numMo);
 else
    plot(its,one_chair_e.*1.005.^one_chair_numMo);
 end
 names = [names, {'one chair'}];
 %axis([0 6 0 200])
 %
 sintel_e = [70.5,58,36.5,27,16,14,14.5,14,14,14.5]; sintel_numMo = [6,11,14,16,16,16,16,17,17,17]-5;
 if semilog
    semilogy(its,sintel_e.*1.005.^sintel_numMo);
 else
    plot(its,sintel_e.*1.005.^sintel_numMo);
 end
 names = [names, {'ambush 5'}];
  
 %
 %figure
 bonn_chair_e = [170.5,159,109,105,103,103,103,97,101,96.5]; bonn_chair_numMo = [6,9,10,10,10,11,11,14,11,12]-5;
 if semilog
    semilogy(its,bonn_chair_e.*1.005.^bonn_chair_numMo);
 else
    plot(its,bonn_chair_e.*1.005.^bonn_chair_numMo);
 end
 names = [names, {'bonn chair'}];
  %legend(names)
 %
 %figure
 bonn_can_e = [208,206.5,183,150,147.5,147,146.5,146,143.5 143.5];
 bonn_can_numMo = [6,7,10,9,10,11,10,11,11,11]-5;
 if semilog
    semilogy(its,bonn_can_e.*1.005.^bonn_can_numMo);
 else
    plot(its,bonn_can_e.*1.005.^bonn_can_numMo);
 end
 names = [names, {'bonn can 1'}];
 

 bonn_can2_e =  [53,48,44.5,43.5,43,43,43,42.5,42.5,42];
  bonn_can2_numMo = [6,7,10,9,10,11,10,11,11,11]-5;
  if semilog
    semilogy(its,bonn_can2_e.*1.005.^bonn_can2_numMo);
  else
    plot(its,bonn_can2_e.*1.005.^bonn_can2_numMo);
  end
 names = [names, {'bonn can 2'}];
  %legend(names)
 
 %
 statue_e = [460,441,360,300,280,260,250,250,250,250]; statue_numMo = [6,10,12,13,12,12,12,12,12,12]-5;
  if semilog
      semilogy(its,statue_e.*1.005.^statue_numMo);
  else
    plot(its,statue_e.*1.005.^statue_numMo);
  end
 names = [names, {'statue'}];
  legend(names)
 
  xlim([1,15])
  xlabel('Iteration')
  ylabel('Energy')
  
  set(gca,'FontName','Times New Roman')
%%
set(out_fig,'Units','Inches');
pos = get(out_fig,'Position');
set(out_fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(out_fig,[out_name,'.pdf'],'-dpdf','-r0')
%%
overall_err = [0.19 0.26 0.23 0.22 0.30 0.26 0.58 0.6 0.61 ;...%chair
0.56 0.79 0.67 0.45 0.61 0.54 0.93 0.94 0.96;...
 0.93 1.00 0.96 0.28 0.52 0.33 0.96 0.98 0.98;...
0.93 1.00 0.96 0.28 0.52 0.33 0.96 0.98 0.98;...
 0.70 0.81 0.78 0.37 0.41 0.41 0.88 0.89 0.90;...%statue
0.74 0.86 0.83 0.61 0.67 0.67 0.94 0.95 0.97;...
 0.42 0.53 0.52 0.28 0.3 0.32 0.92 0.94 0.95;...
0.6 0.83 0.75 0.57 0.59 0.64 0.96 0.98 0.98;...
 0.53 0.75 0.62 0.56 0.69 0.63 0.83 0.92 0.9;...
0.45 0.68 0.53 0.49 0.65 0.55 0.80 0.91 0.88;...
 0.21 0.35 0.28 0.51 0.61 0.57 0.96 0.97 0.98;... %can1
0.42 0.7 0.56 0.82 0.90 0.90 0.95 0.97 0.98;...
 0.40 0.66 0.52 0.89 0.94 0.94 0.83 0.96 0.90;...
0.20 0.59 0.33 0.92 0.98 0.96 0.8 0.98 0.88;...
];

display(sprintf('%.2f &',mean(overall_err(1:2:end,:),1)))
display(sprintf('%.2f &',mean(overall_err(2:2:end,:),1)))

%% runtimes
            %mine       %mo     %seg        EG      IROS
times = [       (11200-751)   (6030)     (11200-751-6030)                 (10*60^2 + 20*60)          (16*60^2 + 25*60)            ;...%bonn_can1 
                (3600-180)   (2364)    (3600-180-2364)                    (3*60^2 + 48*60) ,        (6*60^2 +1*60)   % ;...%bonn_can2                                                    
               (11264-862)  (5040)   (11264-862 -5040)      (11*60^2 +27*60)     (    21*60^2 + 30*60)            ;...%bonn_chair
               (58458 -1486)   (32805) (58458 -1486-32805)  (13*60^2 +30*60)        (4*60^2+25*60) ;...%statue
              (9740 - 500)  5101   (9740 - 500- 5101)       (9*60^2 + 20*60)        ( 4*60^2 + 30 *60)          ;...%one_chair
              (9894 - 567) 6092   (9894 - 567- 6092)        (7*60^2 + 25*60 )        (2*60^2 +40*60  )         ;...%two_chair
              (8076)       (5207)       (8076-   5207)      (1*60^2 + 7*60 )         (1*60^2 +43*60  )        ]%sintel   
                                         
%%

num_frames_used = [60,20,60,220,50,60,20]';
num_frames_used = repmat(num_frames_used,1,5);
num_frames_used(:,5) = [180,60,180,220,50,60,20]';
figure;
subplot(1,2,1);
title('absolute times')
plot(times)
subplot(1,2,2);
plot(times./num_frames_used)
title('per frame times, taking into account when IROS uses more frames')
legend('mine','mine mo', 'mine seg', 'EG','IROS')

figure;
plot(sum(times,1)./sum(num_frames_used,1))

sprintf('%d &',round(sum(times,1)./sum(num_frames_used,1)))