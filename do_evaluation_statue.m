%addToPath
% IROS Results
showSegmentation(15,'G:/PHD/motion_seg_competitors/IROS16_original/motionseg/statue_run/labeling_frame%d.png',num2cell(1899:1899+220));
% my Results
showSegmentation(15,'G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/cast2Eval/labeling_frame%d.png',num2cell(1:220));
%gt
%%
addpath('./gaimc/')
gtLabels = 0:4;%[0:9,9:11];%0:10;
frames_with_gt = 1900:42:2530; 
gtfolder = 'statue_fixed/';
%%
showSummary(15,[gtfolder,'GT_label%d_frame%d.png'],gtLabels, num2cell(frames_with_gt));
showSegmentation(3,[gtfolder,'GT_label%d_frame%d.png'],gtLabels, num2cell(frames_with_gt));

showSegmentation(15,[gtfolder,'/OS_frame%d.png'], num2cell(frames_with_gt));

    
%%
showSegmentation(35,'G:/PHD/motion_seg_competitors/QuingYuan/statue_full_7_2_2017_2000_200_20/result_png/%d.png',num2cell(0:220))

showSegmentation(35,'G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/cast2Eval/labeling_frame%d.png',num2cell(1:220))

%% 

%%

%can allow a label to have multiple matches (i.e. to be oversegmented) by
%listing it multiple times.
%gtLabels = 0:4;%[0:9,9,9:11];%0:10;
rigidLabelIdx = 1:3;
nearlyRigidLabelsIdx = 4:5;
rigidLabels = gtLabels(rigidLabelIdx);
nearlyRigidLabels = gtLabels(nearlyRigidLabelsIdx);
%frames_with_gt = (700-2*24):6:(700+2*24); 

%accuracies etc are only computed over frames where the object is
%visible...
gtVisibleInFrame = true(length(gtLabels), length(frames_with_gt));
gtVisibleInFrame(4, 11:end) = false;
gtVisibleInFrame(5, [1:3,6,11:end]) = false;


gt_seg_file = @(frame, label) sprintf([gtfolder,'GT_label%d_frame%d.png'], label,frame);
%gt_seg_vis_file = @(frame) sprintf([gtfolder,'visualization_summary_frame%d.png'], frame);
gt_consistency_file = @(frame) sprintf([gtfolder,'OS_frame%d.png'], frame);
gt_rigid_nonrgigid_file = @(frame) sprintf([gtfolder,'RN_frame%d.png'], frame);


found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/statue_run/labeling_frame%d.png', 1899+ (frame-1897)/3);
found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/reproduce_statue_endresult/cast2Eval/labeling_frame%d.png', (frame - (1897))/3);
found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/statue_full_7_2_2017_2000_200_20/result_png/%d.png', (frame - (1897))/3);

%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
load_seg_file_IROS = @(frame ) (imread(found_seg_file_IROS(frame)));
load_seg_file_OURS = @(frame ) (imread(found_seg_file_OURS(frame)));
load_seg_file_EG16 = @(frame ) (imread(found_seg_file_EG16(frame)));

%load_seg_file = load_seg_file_IROS;
%load_seg_file = load_seg_file_OURS;
load_gt_seg_file  = @(frame, label) imread(gt_seg_file(frame,label));
load_gt_seg_vis_file  = @(frame) imread(gt_seg_vis_file(frame));
load_gt_consistency_file  = @(frame) imread(gt_consistency_file(frame));

load_gt_seg_file_ignore_nonrigid  = @(frame, label) imread(gt_seg_file(frame,label)).*uint8(imread(gt_rigid_nonrgigid_file(frame))==1);

%to_evaluate = {load_seg_file_IROS, load_seg_file_OURS,load_seg_file_EG16,load_gt_consistency_file};
%to_evaluate_names = {'IROS16', 'OURS', 'EG16', 'dummy'};

to_evaluate = {load_seg_file_IROS,load_seg_file_EG16,load_seg_file_OURS,load_gt_consistency_file};
to_evaluate_names = {'IROS16', 'EG16','OURS', 'dummy'};
is_sparse = {true,true,false, false};

%
 overlaySegmentation(4,load_gt_seg_file,load_seg_file_IROS,gtLabels...
     ,num2cell(2*ones(size(frames_with_gt))),num2cell(4*ones(size(frames_with_gt))), num2cell(frames_with_gt),true, 'test')


%%

for idx = 1:length(to_evaluate)
    load_seg_file = to_evaluate{idx};
    method_name = to_evaluate_names{idx};
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ,...
        gt_l_idx, matching_l_idx] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,gtLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores,...
        frames_with_gt ,gtVisibleInFrame,method_name)

    %temp consistency:
    [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
        extractTemporalStats(load_seg_file, load_gt_consistency_file,frames_with_gt );

    plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

    
    overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell(frames_with_gt),is_sparse{idx}, method_name)
end

 %%
% plotAccuraciesETC( best_accuracies,...
%         best_sensitivities,...
%         best_precisions,...
%         best_Fscores,...
%         frames_with_gt ,gtVisibleInFrame,method_name)
%     %%
 overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell((700-2*24):6:(700+2*24)),idx~=2&& idx~=4, method_name)

%%
for idx = 1:length(to_evaluate)
    %
    %idx = 1;
    load_seg_file = to_evaluate{idx};
     method_name = ['rigid only:',to_evaluate_names{idx}];
%     [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
%         extractFramewiseAccuracyEtc( load_seg_file,...
%         load_gt_seg_file_ignore_nonrigid,...%load_gt_seg_file
%         frames_with_gt,rigidLabels);
    
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,...
        load_gt_seg_file,...
        frames_with_gt,rigidLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores, ...
        frames_with_gt,gtVisibleInFrame(rigidLabelIdx,:) ,method_name)
   
    method_name = ['NEAR rigid only:',to_evaluate_names{idx}];
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,nearlyRigidLabels);

    plotAccuraciesETC( best_accuracies,best_sensitivities,best_precisions,best_Fscores,...
        frames_with_gt ,gtVisibleInFrame(nearlyRigidLabelsIdx,:),method_name)

end

