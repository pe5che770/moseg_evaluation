function varargout = showSegmentation(varargin)
% SHOWSEGMENTATION MATLAB code for showSegmentation.fig
%      SHOWSEGMENTATION, by itself, creates a new SHOWSEGMENTATION or raises the existing
%      singleton*.
%
%      H = SHOWSEGMENTATION returns the handle to a new SHOWSEGMENTATION or the handle to
%      the existing singleton*.
%
%      SHOWSEGMENTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWSEGMENTATION.M with the given input arguments.
%
%      SHOWSEGMENTATION('Property','Value',...) creates a new SHOWSEGMENTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showSegmentation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showSegmentation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showSegmentation

% Last Modified by GUIDE v2.5 06-May-2017 01:15:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showSegmentation_OpeningFcn, ...
                   'gui_OutputFcn',  @showSegmentation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showSegmentation is made visible.
function showSegmentation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showSegmentation (see VARARGIN)


if(length(varargin) ==2)
    handles.fileNames = varargin{1};
     handles.maxNumLabels = varargin{2};
    handles.singleMode = true;
    set(handles.slider1,'Max',length(handles.fileNames));
    numFrames = length(handles.fileNames);
elseif length(varargin) == 3
    handles.file_mask = varargin{2};
    handles.frames = varargin{3};
    handles.maxNumLabels = varargin{1};
    handles.singleMode = false;
    handles.summaryMode = true;
    set(handles.slider1,'Max',length(handles.frames));
     numFrames = length(handles.frames);
elseif length(varargin) == 4
    handles.file_mask = varargin{2};
    handles.labels = varargin{3};
    handles.frames = varargin{4};
    handles.maxNumLabels = varargin{1};
    handles.singleMode = false;
     handles.summaryMode = false;
    set(handles.slider1,'Max',length(handles.frames));
     numFrames = length(handles.frames);
end
set(handles.slider1,'Min',1);
set(handles.slider1,'Value',1);
if(numFrames > 1)
    set(handles.slider1,'SliderStep',[1.0/(numFrames-1) , 10.0/(numFrames-1) ]);
end

%make the slider integer valued
if ~isfield(handles,'hListener')
    handles.hListener = ...
        addlistener(handles.slider1,'ContinuousValueChange',@respondToContSlideCallback);
end
%remember last position
handles.lastSliderVal = 1;

% Choose default command line output for showSegmentation
handles.output = hObject;

slider1_Callback(handles.slider1,eventdata, handles);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showSegmentation wait for user response (see UIRESUME)
% uiwait(handles.figure1);

 % --- Executes on slider movement.
 function respondToContSlideCallback(hObject, eventdata)
 % hObject    handle to slider1 (see GCBO)
 % eventdata  reserved - to be defined in a future version of MATLAB
 % Hints: get(hObject,'Value') returns position of slider
 %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
 % first we need the handles structure which we can get from hObject
 handles = guidata(hObject);
 % get the slider value and convert it to the nearest integer that is less
 % than this value
 newVal = round(get(hObject,'Value'));
 % set the slider value to this integer which will be in the set {1,2,3,...,12,13}
 set(hObject,'Value',newVal);
 % now only do something in response to the slider movement if the 
 % new value is different from the last slider value
 if newVal ~= handles.lastSliderVal
     % it is different, so we have moved up or down from the previous integer
     % save the new value
     handles.lastSliderVal = newVal;
     guidata(hObject,handles);
 end

% --- Outputs from this function are returned to the command line.
function varargout = showSegmentation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function img = create_image(hObject, handles,k)
colors =  double(squeeze(label2rgb(1:handles.maxNumLabels,'jet','k','shuffle')))/255
%colors
if(handles.singleMode)
    imToLoad = k
    imToLoad = handles.fileNames{imToLoad}
    seg = imread(imToLoad);
    img = label2rgb(seg,colors);
else
    img = [];
    frameidx = k
    
    if(~handles.summaryMode)
        idx = 0;
        for l = handles.labels

            imName = sprintf(handles.file_mask, l, handles.frames{frameidx});
            seg = imread(imName);
            ith = mod(idx,4);
            jth = floor(idx/4);
            if  ith == 0
                img_row = ones(size(seg,1), 4*size(seg,2),3);
                img = [img; img_row];
            end
            LRGB = label2rgb(seg,colors);%'jet','w','shuffle');

            img(jth*size(seg,1)+(1:size(seg,1)), ith*size(seg,2) + (1:size(seg,2)),:) = LRGB;
            idx = idx +1;
        end
    else
        imName = sprintf(handles.file_mask, handles.frames{round(frameidx)})
        seg = imread(imName);
        img = label2rgb(seg,colors,'k');
    end
    imshow(img, 'Parent', handles.axes1)
end
    
    
% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

colors =  double(squeeze(label2rgb(1:handles.maxNumLabels,'jet','k','shuffle')))/255
%colors
if(handles.singleMode)
    imToLoad = get(hObject,'Value')
    imToLoad = handles.fileNames{imToLoad}
    seg = imread(imToLoad);
    LRGB = label2rgb(seg,colors);
    imshow(LRGB, 'Parent', handles.axes1)
else
    img = [];
    frameidx = get(hObject,'Value')
    
    if(~handles.summaryMode)
        idx = 0;
        for l = handles.labels

            imName = sprintf(handles.file_mask, l, handles.frames{frameidx});
            seg = imread(imName);
            ith = mod(idx,4);
            jth = floor(idx/4);
            if  ith == 0
                img_row = ones(size(seg,1), 4*size(seg,2),3);
                img = [img; img_row];
            end
            LRGB = label2rgb(seg,colors);%'jet','w','shuffle');

            img(jth*size(seg,1)+(1:size(seg,1)), ith*size(seg,2) + (1:size(seg,2)),:) = LRGB;
            idx = idx +1;
        end
    else
        imName = sprintf(handles.file_mask, handles.frames{round(frameidx)})
        seg = imread(imName);
        img = label2rgb(seg,colors,'k');
    end
    imshow(img, 'Parent', handles.axes1)
end

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

img = create_image(hObject, handles, 1);
img_size = size(img);
size_row_4 = [size(img,1),4*size(img,2),3];
for i = 1:length(handles.frames)
   img = create_image(hObject, handles, i);
   %overall_img(jth*img_size(1) + (1:img_size(1)),ith*img_size(2)+ (1:img_size(2)),:) = img;
   imwrite(img, sprintf('ternary_masks_frame%d.png', handles.frames{i}))
end