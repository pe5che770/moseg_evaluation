%addToPath
addpath('./gaimc/');
% IROS Results
showSegmentation(15,'G:/PHD/motion_seg_competitors/IROS16_original/motionseg/one_chair_run/labeling_frame%d.png',num2cell(652:2:748));
% my Results
showSegmentation(15,'G:/PHD/CRF/recompiled24_11/one_chair/cast2Eval/labeling_frame%d.png',num2cell(652:2:748));
%gt
%
gtLabels = 0:11;%[0:9,9:11];%0:10;
frames_with_gt = (700-2*24):6:(700+2*24); 

showSummary(15,['one_chair3/','GT_label%d_frame%d.png'],gtLabels, num2cell((700-2*24):6:(700+2*24)));
showSegmentation(3,['one_chair3/','GT_label%d_frame%d.png'],gtLabels, num2cell((700-2*24):6:(700+2*24)));

showSegmentation(15,'one_chair3/OS_frame%d.png', num2cell((700-2*24):6:(700+2*24)));

    
%%
showSegmentation(35,'G:/PHD/motion_seg_competitors/QuingYuan/chair_full_7_2_2017_2000_200_20/result_png/%d.png',num2cell(0:49))


%% 

%%

%can allow a label to have multiple matches (i.e. to be oversegmented) by
%listing it multiple times.
gtLabels = 0:11;%[0:9,9,9:11];%0:10;
rigidLabelIdx = 1:3;
nearlyRigidLabelsIdx = 4:12;
rigidLabels = gtLabels(rigidLabelIdx);
nearlyRigidLabels = gtLabels(nearlyRigidLabelsIdx);
frames_with_gt = (700-2*24):6:(700+2*24); 

%accuracies etc are only computed over frames where the object is
%visible...
gtVisibleInFrame = true(length(gtLabels), length(frames_with_gt));
gtVisibleInFrame(8, [4:5,12:end]) = false;
% @(gtLabel) ...
%     iff(gtLabel~= 8,...
%         1:length(frames_with_gt), ...
%         [1:3,6:11]);

gt_seg_file = @(frame, label) sprintf('one_chair3/GT_label%d_frame%d.png', label,frame);
gt_seg_vis_file = @(frame) sprintf('one_chair3/visualization_summary_frame%d.png', frame);
%gt_consistency_file = @(frame) sprintf('one_chair3/combined_frame%d.png', frame);
gt_consistency_file = @(frame) sprintf('one_chair3/OS_frame%d.png', frame);
gt_rigid_nonrgigid_file = @(frame) sprintf('one_chair3/RN_frame%d.png', frame);


found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/one_chair_run/labeling_frame%d.png', frame);
found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/one_chair/cast2Eval/labeling_frame%d.png', frame);
found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/chair_full_7_2_2017_2000_200_20/result_png/%d.png', (frame - (700-2*24))/2+1);

%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
load_seg_file_IROS = @(frame ) (imread(found_seg_file_IROS(frame)));
load_seg_file_OURS = @(frame ) (imread(found_seg_file_OURS(frame)));
load_seg_file_EG16 = @(frame ) (imread(found_seg_file_EG16(frame)));

%load_seg_file = load_seg_file_IROS;
%load_seg_file = load_seg_file_OURS;
load_gt_seg_file  = @(frame, label) imread(gt_seg_file(frame,label));
load_gt_seg_vis_file  = @(frame) imread(gt_seg_vis_file(frame));
load_gt_consistency_file  = @(frame) imread(gt_consistency_file(frame));

load_gt_seg_file_ignore_nonrigid  = @(frame, label) imread(gt_seg_file(frame,label)).*uint8(imread(gt_rigid_nonrgigid_file(frame))==1);

to_evaluate = {load_seg_file_IROS, load_seg_file_OURS,load_seg_file_EG16,load_gt_consistency_file};
to_evaluate_names = {'IROS16', 'OURS', 'EG16', 'dummy'};

overlaySegmentation(4,load_gt_seg_file,load_seg_file_EG16,gtLabels...
     ,num2cell(2*ones(size(frames_with_gt))),num2cell(4*ones(size(frames_with_gt))), num2cell(frames_with_gt),true, 'test')

 %%
% [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
%         extractTemporalStats(load_seg_file_OURS, load_gt_consistency_file,frames_with_gt );
 
     plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

%%

for idx = 1:length(to_evaluate)
    load_seg_file = to_evaluate{idx};
    method_name = to_evaluate_names{idx};
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ,...
        gt_l_idx, matching_l_idx] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,gtLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores,...
        frames_with_gt ,gtVisibleInFrame,method_name)

    %temp consistency:
    [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
        extractTemporalStats(load_seg_file, load_gt_consistency_file,frames_with_gt );

    plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

    
    overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell((700-2*24):6:(700+2*24)),idx~=2&& idx~=4, method_name)
end

 %%
% plotAccuraciesETC( best_accuracies,...
%         best_sensitivities,...
%         best_precisions,...
%         best_Fscores,...
%         frames_with_gt ,gtVisibleInFrame,method_name)
%     %%
 overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell((700-2*24):6:(700+2*24)),idx~=2&& idx~=4, method_name)

%%
for idx = 1:length(to_evaluate)
     load_seg_file = to_evaluate{idx};
     method_name = ['rigid only:',to_evaluate_names{idx}];
%      [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
%          extractFramewiseAccuracyEtc( load_seg_file,...
%          load_gt_seg_file_ignore_nonrigid,...%load_gt_seg_file
%          frames_with_gt,rigidLabels);
    
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,...
        load_gt_seg_file,...
        frames_with_gt,rigidLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores, ...
        frames_with_gt,gtVisibleInFrame(rigidLabelIdx,:) ,method_name)
    
    method_name = ['NEAR rigid only:',to_evaluate_names{idx}];
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,nearlyRigidLabels);

    plotAccuraciesETC( best_accuracies,best_sensitivities,best_precisions,best_Fscores,...
        frames_with_gt ,gtVisibleInFrame(nearlyRigidLabelsIdx,:),method_name)

end

