%%
%700 +-24*2
frames = {651,681};
%color_filename_mask = 'col%05d.png';
%depth_filename_mask = 'depth%05d.png';
depth_filename_mask = 'G:/PHD/DATA/Data/oldsequence_full_bonnFormat/chair_3/chair_3_cast_long/depth/%05d.png';
color_filename_mask = 'G:/PHD/DATA/Data/oldsequence_full_bonnFormat/chair_3/chair_3_cast_long/rgb/%05d.png';
output_folder = 'one_chair3/';

depth_to_m = 0.0002;
%used files:
%[700-2*24 +[0-48]*2]
%frames = num2cell((700-2*24):2:(700+2*24));
frames = num2cell((700-2*24):6:(700+2*24));
%col = sprintf(color_filename_mask, 700-2*24 +8*2)
%col = imread(col);
%figure;imshow(col);

%color_files = {'col00651.png'};
%depth_files = {'depth00651.png'};

%% ground truth for label 1.
label = 5;%4;
figure_index = 1;
results = {};

%use prior positive labels as negatives!
while figure_index <= length(frames)
    
    frame = frames{figure_index};

    %read color and depth
    col_file = sprintf(color_filename_mask, frame);
    depth_file = sprintf(depth_filename_mask, frame);
    col = imread(col_file);
    depth = imread(depth_file);
    depth = double(depth)*depth_to_m;

    %plan: do every 30th frame- 30 = 2*3*5 (not enough! 2*3) => allows sampling every 2nd,
    %3rd,5th,10th,15th frame while still being able to use all gt.

   % known_negatives = false(size(depth));
   % for old_labels = 1: label-1
   %    labeling_last_label = [output_folder,'GT_label',num2str(old_label),'_frame',num2str(frame),'.png'];
   %    labeling = imread(labeling_last_label);
   %    known_negatives = known_negatives || labeling ==1;
   % end
    if(length(results) > 0)
       h =  showSegmentation(results,3);
    end
    if(figure_index == 1)
        fh= gt_seg_gui(col, depth,[output_folder,'GT_label',num2str(label),'_frame',num2str(frame)], '');%'constraints.png');
    else
        last_frame= frames{figure_index-1};
        last_col_file = sprintf(color_filename_mask, last_frame);
        %propagate labeling to frame...
        initial_guess_labeling = [output_folder,'GT_label',num2str(label),'_frame',num2str(last_frame),'_constraints.png'];
        fh= gt_seg_gui(col, depth,[output_folder,'GT_label',num2str(label),'_frame',num2str(frame)], initial_guess_labeling);
    end
    waitfor(fh);
    disp('ok');
       
    if(length(results) > 0)
       delete(h);
    end
    results = [results, {[output_folder,'GT_label',num2str(label),'_frame',num2str(frame),'.png']}]
    figure_index = figure_index+1;
    
end
%% refine....
showSegmentation(results,3);
figure_index = 1;
%%
while figure_index <= length(frames)
    
    frame = frames{figure_index};

    %read color and depth
    col_file = sprintf(color_filename_mask, frame);
    depth_file = sprintf(depth_filename_mask, frame);
    col = imread(col_file);
    depth = imread(depth_file);
    depth = double(depth)*depth_to_m;

       
    initial_guess_labeling = [output_folder,'GT_label',num2str(label),'_frame',num2str(frame),'_constraints.png'];
    fh= gt_seg_gui(col, depth,[output_folder,'GT_label',num2str(label),'_frame',num2str(frame)], initial_guess_labeling);
    waitfor(fh);
    disp('ok');
    figure_index = figure_index+1;
    
end

%% show all
first_label = 0;
last_label = 11;
showSegmentation(3,[output_folder,'GT_label%d_frame%d.png'],first_label:last_label, frames);

%%
showSummary(last_label,[output_folder,'GT_label%d_frame%d.png'],first_label:last_label, frames);

%% rigid non rigid mask
%
figure_index = 1;
results = {};
refine = true;
while figure_index <= length(frames)
    
    frame = frames{figure_index};

    %read color and depth
    col_file = sprintf(color_filename_mask, frame);
    depth_file = sprintf(depth_filename_mask, frame);
    col = imread(col_file);
    depth = imread(depth_file);
    depth = double(depth)*depth_to_m;

    if(length(results) > 0)
       h =  showSegmentation(results,20);
    end
    if(figure_index == 1)
        
        if refine
            initial_guess_labeling = [output_folder,'RN_frame',num2str(frame),'_constraints.png'];
        else
            initial_guess_labeling ='';
        end
        fh= gt_seg_gui(col, depth,[output_folder,'RN_frame',num2str(frame)],initial_guess_labeling);%'constraints.png');
    else
        last_frame= frames{figure_index-1};
        last_col_file = sprintf(color_filename_mask, last_frame);
        %propagate labeling to frame...
        if refine
            initial_guess_labeling = [output_folder,'RN_frame',num2str(frame),'_constraints.png'];
        else
            initial_guess_labeling = [output_folder,'RN_frame',num2str(last_frame),'_constraints.png'];
        end
        fh= gt_seg_gui(col, depth,[output_folder,'RN_frame',num2str(frame)], initial_guess_labeling);
    end
    waitfor(fh);
    disp('ok');
       
    if(length(results) > 0)
       delete(h);
    end
    results = [results, {[output_folder,'RN_frame',num2str(frame),'.png']}]
    figure_index = figure_index+1;
    
end

results_all = results;
showSegmentation(results,3);

%% oversegmentation from normal segmentation

for frameidx = 1:length(frames)
imName = sprintf([output_folder,'GT_label%d_frame%d.png'], 1, frames{1});
seg = imread(imName);
summary_seg = zeros(size(seg));
for l = first_label:last_label
    imName = sprintf([output_folder,'GT_label%d_frame%d.png'], l, frames{frameidx});
    seg = imread(imName);
    summary_seg(seg == 1) = l+1;
end
imwrite(uint8(summary_seg),sprintf([output_folder,'combined_frame%d.png'], frames{frameidx}))
figure;
imshow(label2rgb(summary_seg));
end

%% over segmentation

figure_index = 1;
results = {};

%use prior positive labels as negatives!
while figure_index <= length(frames)
    
    frame = frames{figure_index};

    %read color and depth
    col_file = sprintf(color_filename_mask, frame);
    depth_file = sprintf(depth_filename_mask, frame);
    col = imread(col_file);
    depth = imread(depth_file);
    depth = double(depth)*depth_to_m;

    %plan: do every 30th frame- 30 = 2*3*5 (not enough! 2*3) => allows sampling every 2nd,
    %3rd,5th,10th,15th frame while still being able to use all gt.

   % known_negatives = false(size(depth));
   % for old_labels = 1: label-1
   %    labeling_last_label = [output_folder,'GT_label',num2str(old_label),'_frame',num2str(frame),'.png'];
   %    labeling = imread(labeling_last_label);
   %    known_negatives = known_negatives || labeling ==1;
   % end
    if(length(results) > 0)
       h =  showSegmentation(results,20);
    end
    if(figure_index == 1)
        fh= gt_seg_gui(col, depth,[output_folder,'OS_frame',num2str(frame)], [output_folder,'combined_frame',num2str(frame), '.png']);%'constraints.png');
    else
        last_frame= frames{figure_index-1};
        last_col_file = sprintf(color_filename_mask, last_frame);
        %propagate labeling to frame...
        initial_guess_labeling = [output_folder,'combined_frame',num2str(frame),'.png'];%[output_folder,'OS_frame',num2str(last_frame),'_constraints.png'];
        fh= gt_seg_gui(col, depth,[output_folder,'OS_frame',num2str(frame)], initial_guess_labeling);
    end
    waitfor(fh);
    disp('ok');
       
    if(length(results) > 0)
       delete(h);
    end
    results = [results, {[output_folder,'OS_frame',num2str(frame),'.png']}]
    figure_index = figure_index+1;
    
end

results_all = results;
%%

showSegmentation(results_all,19);

%% refine
figure_index = 1;
results = {};

%use prior positive labels as negatives!
while figure_index <= length(frames)
    
    frame = frames{figure_index};

    %read color and depth
    col_file = sprintf(color_filename_mask, frame);
    depth_file = sprintf(depth_filename_mask, frame);
    col = imread(col_file);
    depth = imread(depth_file);
    depth = double(depth)*depth_to_m;

    %plan: do every 30th frame- 30 = 2*3*5 (not enough! 2*3) => allows sampling every 2nd,
    %3rd,5th,10th,15th frame while still being able to use all gt.

   % known_negatives = false(size(depth));
   % for old_labels = 1: label-1
   %    labeling_last_label = [output_folder,'GT_label',num2str(old_label),'_frame',num2str(frame),'.png'];
   %    labeling = imread(labeling_last_label);
   %    known_negatives = known_negatives || labeling ==1;
   % end

        %propagate labeling to frame...
        initial_guess_labeling = [output_folder,'OS_frame',num2str(frame),'_constraints.png'];
        fh= gt_seg_gui(col, depth,[output_folder,'OS_frame',num2str(frame)], initial_guess_labeling);
    waitfor(fh);
    disp('ok');

    results = [results, {[output_folder,'OS_frame',num2str(frame),'.png']}]
    figure_index = figure_index+1; 
end