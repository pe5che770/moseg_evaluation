function varargout = gt_seg_gui(varargin)
% GT_SEG_GUI MATLAB code for gt_seg_gui.fig
%      GT_SEG_GUI, by itself, creates a new GT_SEG_GUI or raises the existing
%      singleton*.
%
%      H = GT_SEG_GUI returns the handle to a new GT_SEG_GUI or the handle to
%      the existing singleton*.
%
%      GT_SEG_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GT_SEG_GUI.M with the given input arguments.
%
%      GT_SEG_GUI('Property','Value',...) creates a new GT_SEG_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gt_seg_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gt_seg_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gt_seg_gui

% Last Modified by GUIDE v2.5 24-Mar-2017 09:52:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gt_seg_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gt_seg_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gt_seg_gui is made visible.
function gt_seg_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gt_seg_gui (see VARARGIN)

%segmentation data
handles.color = varargin{1};
handles.depth = varargin{2};
handles.out_base_name ='';
handles.in_constraint_file_name = '';
if nargin > 2
    handles.out_base_name = varargin{3};
end
if nargin > 3
    handles.in_constraint_file_name = varargin{4};
end

%label management
handles.labels = {1,2};
handles.masks = {false(size(handles.depth)); false(size(handles.depth))};
handles.unusedLabel = 3;
set(handles.listbox1, 'String', ['1';'2']);


%robustness to missing data via simple hack,
%compute the graphcuts smoothnessterm
d2 = regionfill(handles.depth,(handles.depth == 0));
[dx,dy] = gradient(d2);
gradMagnitude = (dx.^2 + dy.^2);
depth_smoothness = exp(-gradMagnitude/mean(gradMagnitude(:)));

intensity = mean(handles.color,3);
intensity = medfilt2(intensity,[5,5]);
[dx,dy] = gradient(intensity);
col_smoothness = dx.^2 + dy.^2;
col_smoothness = log(1+ col_smoothness);
col_smoothness = 1-col_smoothness/max(col_smoothness(:));

%works:
%handles.smoothness = exp(-gradMagnitude/mean(gradMagnitude(:)));
%trying:
handles.smoothness = min(depth_smoothness ,col_smoothness);

%more extreme...
handles.smoothness = max(handles.smoothness.^4,0);
handles.smoothness = imgaussfilt(handles.smoothness,0.25);
%filtered = imgaussfilt(handles.smoothness,0.5);
%handles.smoothness(handles.smoothness<0.05) = filtered(handles.smoothness < 0.05);
%show something
imshow(handles.color,'Parent',handles.axes1)
imshow(handles.smoothness/max(handles.smoothness(:)),'Parent',handles.axes2)
%figure; imshow(handles.smoothness)

% Choose default command line output for gt_seg_gui
handles.output = hObject;

updateListboxEntries(hObject, handles)
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = gt_seg_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function updateListboxEntries(hObject, handles)
%update label listbox entries.
new_name = {};
alllabels = handles.labels;
cols = label2rgb(1:length(alllabels), 'jet', 'w', 'shuffle');
for i=1:length(alllabels)
    selected_label = alllabels{i};
    col = squeeze(cols(:,i,:));

    htmlname = sprintf('<HTML><BODY bgcolor="rgb(%d,%d,%d)">%s',col(1), col(2),col(3), '____');
    new_name = [new_name;{htmlname}];
end
set(handles.listbox1, 'String', new_name);


% --- Executes on button press in addButton.
function addButton_Callback(hObject, eventdata, handles)
% hObject    handle to addButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.labels{length(handles.labels)+1} = handles.unusedLabel;
handles.unusedLabel = handles.unusedLabel +1;
handles.masks = [handles.masks; false(size(handles.depth))];

updateListboxEntries(hObject, handles)
updateSelectionDisplay(handles)
% Update handles structure
guidata(hObject, handles);

%a function to update the constraint display
function updateSelectionDisplay(handles)
%selected_label = handles.listbox1.Value;
masked_imgx = handles.color(:,:,1);
masked_imgy = handles.color(:,:,2);
masked_imgz = handles.color(:,:,3);
alllabels = handles.labels;

cols = label2rgb(1:length(alllabels), 'jet', 'w', 'shuffle');
for i=1:length(alllabels)
    selected_label = alllabels{i};
    col = squeeze(cols(:,i,:));
    masked_imgx(handles.masks{i}) = col(1);
    masked_imgy(handles.masks{i}) = col(2);
    masked_imgz(handles.masks{i}) = col(3);
end
masked(:,:,1) = masked_imgx;
masked(:,:,2) = masked_imgy;
masked(:,:,3) = masked_imgz;
imshow(masked,'Parent',handles.axes1)


function do_graphcut_and_updateHandles(hObject, handles)

numLabels = length(handles.labels);
smoothness = ones(numLabels) - eye(numLabels,numLabels);
numConstraints = 0;
for i = 1: numLabels
    if(sum(handles.masks{i}(:)) > 0)
        numConstraints = numConstraints +1;
    end
   dataterm(:,:,i) =  -log(0.95*double(handles.masks{i}) + 0.05);
end

if(numConstraints >1)
    [gch] = GraphCut('open', dataterm, smoothness, handles.smoothness, handles.smoothness);

    iter = 5;
    [gch, labels] = GraphCut('swap', gch, iter);
     [gch] = GraphCut('close', gch);
    clear gch
    labels = labels +1;
    labels(handles.depth == 0) = 0;
    
    %display
    Lrgb = label2rgb(labels, 'jet', 'w', 'shuffle');
    imshow(Lrgb,'Parent',handles.axes2)
    handles.output_labeling = labels;
end
guidata(hObject, handles);


% --- Executes on button press in erasebutton.
function erasebutton_Callback(hObject, eventdata, handles)
% hObject    handle to erasebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = imfreehand(handles.axes1);
mask1 = createMask(h);
selected_label = handles.listbox1.Value;
handles.masks{selected_label} = handles.masks{selected_label} & ~mask1;

updateSelectionDisplay(handles)
guidata(hObject, handles);



% --- Executes on button press in drawbutton.
function drawbutton_Callback(hObject, eventdata, handles)
% hObject    handle to drawbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = imfreehand(handles.axes1);
mask1 = createMask(h);
selected_label = handles.listbox1.Value;
handles.masks{selected_label} = handles.masks{selected_label}|mask1;

updateSelectionDisplay(handles)
do_graphcut_and_updateHandles(hObject,handles)

%guidata(hObject, handles);



% --- Executes on button press in deleteButton.
function deleteButton_Callback(hObject, eventdata, handles)
% hObject    handle to deleteButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject); 



% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%save constraints and labeling.

if(length(handles.out_base_name) > 0)
    constraints = handles.masks;
    u8_grayscale = uint8(zeros(size(constraints{1})));
    for i = 1: length(constraints)
        u8_grayscale(constraints{i}) = i;
    end
    imwrite(u8_grayscale, [handles.out_base_name, '_constraints.png']) ;

    labeling = handles.output_labeling;
    u8_grayscale = uint8(zeros(size(labeling)));
    for i = 1: max(labeling(:))
        u8_grayscale(labeling == i) = i;
    end
    imwrite(u8_grayscale, [handles.out_base_name,'.png']) ;
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%load constraints.
if length(handles.in_constraint_file_name) > 0
    u8_grayscale = imread(handles.in_constraint_file_name);
    numLabels = max(u8_grayscale(:));
    
    handles.labels = {};
    handles.masks ={};
    for i = 1:numLabels
        handles.labels{i} = i;
        handles.masks = [handles.masks; u8_grayscale == i];
    end
    handles.unusedLabel = numLabels +1;
    
    updateListboxEntries(hObject, handles)
    updateSelectionDisplay(handles)
    do_graphcut_and_updateHandles(hObject,handles);
end