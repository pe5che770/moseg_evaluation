
%alternative: imseggeodesic

alpha_smoothnes = 0.01;

numLabels = 2;
height = 30;width = 60;
clear dataterm
dataterm(:,:,1) = 0.48*ones(height,width);
dataterm(:,1:(width/2)) = 0.52;
dataterm(:,:,2) = 1-dataterm(:,:,1);

smoothness = ones(numLabels) - eye(numLabels,numLabels); %spatially unvarrying.
vertical_smoothness = alpha_smoothnes *ones(height,width);%(i,j)->(i+1,j)
horizontal_smoothness = alpha_smoothnes *ones(height,width);%(i,j)->(i+1,j)


[gridx, gridy] = meshgrid(1:width,1:height);
horizontal_smoothness((gridx-width/2).^2 + (gridy-height/2).^2 < 55) = 10;
vertical_smoothness = horizontal_smoothness;
figure; surf(horizontal_smoothness)

%%

[gch] = GraphCut('open', dataterm, smoothness, vertical_smoothness, horizontal_smoothness);

%[gch] = GraphCut('set', gch, labels) %initial guess?

iter = 10;
[gch, labels] = GraphCut('swap', gch, iter);

Lrgb = label2rgb(labels, 'jet', 'w', 'shuffle');

figure;
imshow(Lrgb)


%[gch labels] = GraphCut('get', gch);

[gch] = GraphCut('close', gch);
clear gch;