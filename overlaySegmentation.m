function varargout = overlaySegmentation(varargin)
% SHOWSEGMENTATION MATLAB code for showSegmentation.fig
%      SHOWSEGMENTATION, by itself, creates a new SHOWSEGMENTATION or raises the existing
%      singleton*.
%
%      H = SHOWSEGMENTATION returns the handle to a new SHOWSEGMENTATION or the handle to
%      the existing singleton*.
%
%      SHOWSEGMENTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWSEGMENTATION.M with the given input arguments.
%
%      SHOWSEGMENTATION('Property','Value',...) creates a new SHOWSEGMENTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showSegmentation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showSegmentation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showSegmentation

% Last Modified by GUIDE v2.5 07-Apr-2017 14:25:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @overlaySegmentation_OpeningFcn, ...
                   'gui_OutputFcn',  @overlaySegmentation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showSegmentation is made visible.
function overlaySegmentation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showSegmentation (see VARARGIN)


    handles.maxNumLabels = varargin{1};
    handles.loadGtSeg = varargin{2};
    handles.loadSeg2 = varargin{3};
    handles.gtlabels = varargin{4};
    handles.gtLabelIdx = varargin{5};
    handles.labels2Idx = varargin{6};
    handles.frames = varargin{7};
    handles.isSparse = varargin{8};
    handles.title = varargin{9};

    handles.singleMode = false;
     handles.summaryMode = false;
    set(handles.slider1,'Max',length(handles.frames));
     numFrames = length(handles.frames);

set(handles.slider1,'Min',1);
set(handles.slider1,'Value',1);
if(numFrames > 1)
    set(handles.slider1,'SliderStep',[1.0/(numFrames-1) , 10.0/(numFrames-1) ]);
end

%make the slider integer valued
if ~isfield(handles,'hListener')
    handles.hListener = ...
        addlistener(handles.slider1,'ContinuousValueChange',@respondToContSlideCallback);
end
%remember last position
handles.lastSliderVal = 1;

% Choose default command line output for showSegmentation
handles.output = hObject;

slider1_Callback(handles.slider1,eventdata, handles);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showSegmentation wait for user response (see UIRESUME)
% uiwait(handles.figure1);

 % --- Executes on slider movement.
 function respondToContSlideCallback(hObject, eventdata)
 % hObject    handle to slider1 (see GCBO)
 % eventdata  reserved - to be defined in a future version of MATLAB
 % Hints: get(hObject,'Value') returns position of slider
 %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
 % first we need the handles structure which we can get from hObject
 handles = guidata(hObject);
 % get the slider value and convert it to the nearest integer that is less
 % than this value
 newVal = round(get(hObject,'Value'));
 % set the slider value to this integer which will be in the set {1,2,3,...,12,13}
 set(hObject,'Value',newVal);
 % now only do something in response to the slider movement if the 
 % new value is different from the last slider value
 if newVal ~= handles.lastSliderVal
     % it is different, so we have moved up or down from the previous integer
     % save the new value
     handles.lastSliderVal = newVal;
     guidata(hObject,handles);
 end

% --- Outputs from this function are returned to the command line.
function varargout = overlaySegmentation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%            handles.maxNumLabels = varargin{1};
%     handles.loadGtSeg = varargin{2};
%     handles.loadSeg2 = varargin{3};
%     handles.gtlabels = varargin{4};
%     handles.gtLabelIdx = varargin{5};
%     handles.labels2Idx = varargin{6};
%     handles.frames = varargin{7};
%     handles.isSparse = varargin{8}; 
    
colors =  double(squeeze(label2rgb(1:handles.maxNumLabels,'jet','k','shuffle')))/255;
%colors
idx = 0;
img = [];
frame_idx = get(hObject,'Value');
frame = handles.frames{frame_idx};

%init the summary image
for gt_l_idx = 1:length(handles.gtlabels)
    gt_l = handles.gtlabels(gt_l_idx);
    seg = handles.loadGtSeg(frame,gt_l);
    ith = mod(gt_l_idx-1,4);
    jth = floor((gt_l_idx-1)/4);
    if  ith == 0
        img_row = ones(size(seg,1), 4*size(seg,2),3);
        img = [img; img_row];
    end
    LRGB = label2rgb(seg,colors);%'jet','w','shuffle');
    img(jth*size(seg,1)+(1:size(seg,1)), ith*size(seg,2) + (1:size(seg,2)),:) = LRGB;
end

for l_idx = 1:length(handles.labels2Idx{frame_idx})
    gt_l_idx = handles.gtLabelIdx{frame_idx}(l_idx);
    l2_idx = handles.labels2Idx{frame_idx}(l_idx);
    gt_l = handles.gtlabels(gt_l_idx);
    seg = handles.loadGtSeg(frame,gt_l);
    seg2 = handles.loadSeg2(frame);
    ith = mod(gt_l_idx-1,4);
    jth = floor((gt_l_idx-1)/4);
%     if  ith == 0
%         img_row = ones(size(seg,1), 4*size(seg,2),3);
%         img = [img; img_row];
%     end
    
    who = seg2 == l2_idx;
    if(~handles.isSparse)
        none = false(size(who));
        %meshgrid
        none(1:10:end, 1:10:end) = true;
        who(~none) =  false;
    end
    %else
        who = (imdilate(who, ones(3)) == 1);
    %end
    seg(who) = (who(who) )*4;
    LRGB = label2rgb(seg,colors);%'jet','w','shuffle');

    img(jth*size(seg,1)+(1:size(seg,1)), ith*size(seg,2) + (1:size(seg,2)),:) = LRGB;
    idx = idx +1;
end

imshow(img, 'Parent', handles.axes1)
title(handles.title)


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
