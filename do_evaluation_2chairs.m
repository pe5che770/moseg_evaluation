%addToPath
addpath('./gaimc/');
% IROS Results
showSegmentation(15,'G:/PHD/motion_seg_competitors/IROS16_original/motionseg/my_two_chairs_run/labeling_frame%d.png',num2cell(0:58));
% my Results
showSegmentation(25,'G:/PHD/CRF/recompiled24_11/two_chairs/cast2eval/labeling_frame%d.png',num2cell(0:58));
%% EG
showSegmentation(35,'G:/PHD/motion_seg_competitors/QuingYuan/two_chairs_full_7_2_2017_2000_200_20/result_png/%d.png',num2cell(0:57));
%G:/PHD/motion_seg_competitors/QuingYuan/bonn_chair_full_7_2_2017_2000_200_20
%gt
%%
gtLabels = 0:2;%[0:9,9:11];%0:10;
rigidLabelIdx = 1:3;
nearlyRigidLabelsIdx = [];
frames_with_gt = 10:10:50; 
gt_folder = 'my_two_chairs/';

showSummary(15,[gt_folder,'GT_label%d_frame%d.png'],gtLabels, num2cell(frames_with_gt));
showSegmentation(3,[gt_folder,'GT_label%d_frame%d.png'],gtLabels, num2cell(frames_with_gt));

%showSegmentation(15,[gt_folder,'OS_frame%d.png'], num2cell(frames_with_gt));

    
%%
%showSegmentation(35,'G:/PHD/motion_seg_competitors/QuingYuan/chair_full_7_2_2017_2000_200_20/result_png/%d.png',num2cell(0:49))

 %overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell((700-2*24):6:(700+2*24)),idx~=2&& idx~=4, method_name)
% overlaySegmentation(4,load_gt_seg_file,load_seg_file_OURS,gtLabels...
%     ,num2cell(ones(size(frames_with_gt))),num2cell(ones(size(frames_with_gt))), num2cell(frames_with_gt),false, 'test')
%% 

%%

%can allow a label to have multiple matches (i.e. to be oversegmented) by
%listing it multiple times.
%gtLabels = 0:11;%[0:9,9,9:11];%0:10;

rigidLabels = gtLabels(rigidLabelIdx);
nearlyRigidLabels = gtLabels(nearlyRigidLabelsIdx);
%frames_with_gt = (700-2*24):6:(700+2*24); 

%accuracies etc are only computed over frames where the object is
%visible...
gtVisibleInFrame = true(length(gtLabels), length(frames_with_gt));

gt_seg_file = @(frame, label) sprintf([gt_folder,'GT_label%d_frame%d.png'], label,frame);
gt_seg_vis_file = @(frame) sprintf([gt_folder,'visualization_summary_frame%d.png'], frame);
%gt_consistency_file = @(frame) sprintf('one_chair3/combined_frame%d.png', frame);
gt_consistency_file = @(frame) sprintf([gt_folder,'OS_frame%d.png'], frame);
gt_rigid_nonrgigid_file = @(frame) sprintf([gt_folder,'/RN_frame%d.png'], frame);


found_seg_file_IROS = @(frame) sprintf('G:/PHD/motion_seg_competitors/IROS16_original/motionseg/my_two_chairs_run/labeling_frame%d.png', frame);
found_seg_file_OURS = @(frame) sprintf('G:/PHD/CRF/recompiled24_11/two_chairs/cast2eval/labeling_frame%d.png', frame-1);
found_seg_file_EG16 = @(frame) sprintf('G:/PHD/motion_seg_competitors/QuingYuan/two_chairs_full_7_2_2017_2000_200_20/result_png/%d.png', frame);

%load_seg_file = @(frame ) flip(imread(found_seg_file(frame)),2);
load_seg_file_IROS = @(frame ) (imread(found_seg_file_IROS(frame)));
load_seg_file_OURS = @(frame ) (imread(found_seg_file_OURS(frame)));
load_seg_file_EG16 = @(frame ) (imread(found_seg_file_EG16(frame)));

%load_seg_file = load_seg_file_IROS;
%load_seg_file = load_seg_file_OURS;
load_gt_seg_file  = @(frame, label) imread(gt_seg_file(frame,label));
load_gt_seg_vis_file  = @(frame) imread(gt_seg_vis_file(frame));
load_gt_consistency_file  = @(frame) imread(gt_consistency_file(frame));

load_gt_seg_file_ignore_nonrigid  = @(frame, label) imread(gt_seg_file(frame,label)).*uint8(imread(gt_rigid_nonrgigid_file(frame))==1);

%to_evaluate = {load_seg_file_IROS, load_seg_file_OURS,load_seg_file_EG16,load_gt_consistency_file};
%to_evaluate_names = {'IROS16', 'OURS', 'EG16', 'dummy'};
to_evaluate = {load_seg_file_IROS,load_seg_file_EG16,load_seg_file_OURS,load_gt_consistency_file};
to_evaluate_names = { 'IROS16','EG16','OURS', 'dummy'};
is_sparse = [true,true,false,false];

 %
% [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
%         extractTemporalStats(load_seg_file_OURS, load_gt_consistency_file,frames_with_gt );
 
  %   plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

%%

for idx = 1:length(to_evaluate)
    load_seg_file = to_evaluate{idx};
    method_name = to_evaluate_names{idx};
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ,...
        gt_l_idx, matching_l_idx] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,gtLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores,...
        frames_with_gt ,gtVisibleInFrame,method_name)

    %temp consistency:
    [num_temp_errors_per_label,num_temp_errors_per_pixel ] = ...
        extractTemporalStats(load_seg_file, load_gt_consistency_file,frames_with_gt );

    plotTempConsistency(num_temp_errors_per_label,num_temp_errors_per_pixel,frames_with_gt,method_name)

    
    overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell(frames_with_gt),is_sparse(idx), method_name)
end

 %
% plotAccuraciesETC( best_accuracies,...
%         best_sensitivities,...
%         best_precisions,...
%         best_Fscores,...
%         frames_with_gt ,gtVisibleInFrame,method_name)
%     %%
 %overlaySegmentation(4,load_gt_seg_file,load_seg_file,gtLabels,gt_l_idx,matching_l_idx, num2cell(frames_with_gt),is_sparse(idx), method_name)

%%
for idx = 1:length(to_evaluate)
     load_seg_file = to_evaluate{idx};
     method_name = ['rigid only:',to_evaluate_names{idx}];
%      [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
%          extractFramewiseAccuracyEtc( load_seg_file,...
%          load_gt_seg_file_ignore_nonrigid,...%load_gt_seg_file
%          frames_with_gt,rigidLabels);
    
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,...
        load_gt_seg_file,...
        frames_with_gt,rigidLabels);

    plotAccuraciesETC( best_accuracies,...
        best_sensitivities,...
        best_precisions,...
        best_Fscores, ...
        frames_with_gt,gtVisibleInFrame(rigidLabelIdx,:) ,method_name)
    
    method_name = ['NEAR rigid only:',to_evaluate_names{idx}];
    [~,~,~,~,best_accuracies,best_sensitivities,best_precisions,best_Fscores ] = ...
        extractFramewiseAccuracyEtc( load_seg_file,load_gt_seg_file,frames_with_gt,nearlyRigidLabels);

    plotAccuraciesETC( best_accuracies,best_sensitivities,best_precisions,best_Fscores,...
        frames_with_gt ,gtVisibleInFrame(nearlyRigidLabelsIdx,:),method_name)

end

