function varargout = showSummary(varargin)
% SHOWSUMMARY MATLAB code for showSummary.fig
%      SHOWSUMMARY, by itself, creates a new SHOWSUMMARY or raises the existing
%      singleton*.
%
%      H = SHOWSUMMARY returns the handle to a new SHOWSUMMARY or the handle to
%      the existing singleton*.
%
%      SHOWSUMMARY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWSUMMARY.M with the given input arguments.
%
%      SHOWSUMMARY('Property','Value',...) creates a new SHOWSUMMARY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showSummary_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showSummary_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showSummary

% Last Modified by GUIDE v2.5 27-Mar-2017 13:46:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showSummary_OpeningFcn, ...
                   'gui_OutputFcn',  @showSummary_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showSummary is made visible.
function showSummary_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showSummary (see VARARGIN)


if(length(varargin) ==2)
    handles.fileNames = varargin{1};
     handles.maxNumLabels = varargin{2};
    handles.singleMode = true;
    set(handles.slider1,'Max',length(handles.fileNames));
    numFrames = length(handles.fileNames);
elseif length(varargin) == 4
    handles.file_mask = varargin{2};
    handles.labels = varargin{3};
    handles.frames = varargin{4};
    handles.maxNumLabels = varargin{1};
    handles.singleMode = false;
    set(handles.slider1,'Max',length(handles.frames));
     numFrames = length(handles.frames);
end
set(handles.slider1,'Min',1);
set(handles.slider1,'Value',1);
if(numFrames > 1)
    set(handles.slider1,'SliderStep',[1.0/(numFrames-1) , 10.0/(numFrames-1) ]);
end

%make the slider integer valued
if ~isfield(handles,'hListener')
    handles.hListener = ...
        addlistener(handles.slider1,'ContinuousValueChange',@respondToContSlideCallback);
end
%remember last position
handles.lastSliderVal = 1;

% Choose default command line output for showSummary
handles.output = hObject;

slider1_Callback(handles.slider1,eventdata, handles);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showSummary wait for user response (see UIRESUME)
% uiwait(handles.figure1);

 % --- Executes on slider movement.
 function respondToContSlideCallback(hObject, eventdata)
 % hObject    handle to slider1 (see GCBO)
 % eventdata  reserved - to be defined in a future version of MATLAB
 % Hints: get(hObject,'Value') returns position of slider
 %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
 % first we need the handles structure which we can get from hObject
 handles = guidata(hObject);
 % get the slider value and convert it to the nearest integer that is less
 % than this value
 newVal = round(get(hObject,'Value'));
 % set the slider value to this integer which will be in the set {1,2,3,...,12,13}
 set(hObject,'Value',newVal);
 % now only do something in response to the slider movement if the 
 % new value is different from the last slider value
 if newVal ~= handles.lastSliderVal
     % it is different, so we have moved up or down from the previous integer
     % save the new value
     handles.lastSliderVal = newVal;
     guidata(hObject,handles);
 end

% --- Outputs from this function are returned to the command line.
function varargout = showSummary_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%-----

function  img = createImage(hObject, handles, frameidx)
    colors =  double(squeeze(label2rgb(1:(handles.maxNumLabels+1),'jet','w','shuffle')))/255;
    
    %init.
    imName = sprintf(handles.file_mask, handles.labels(1), handles.frames{frameidx});
    seg = imread(imName);
    img_pos = ones(size(seg,1), size(seg,2),3);
    mask_pos =false(size(seg,1), size(seg,2));
    mask_neutral =false(size(seg,1), size(seg,2));
    
    summary_seg = zeros(size(seg));
    for l = handles.labels
        imName = sprintf(handles.file_mask, l, handles.frames{frameidx});
        seg = imread(imName);
        mask_pos = mask_pos | seg==1;
        mask_neutral = mask_neutral | seg==3;
        summary_seg(seg == 1) = l+1;
    end
    img_pos  = label2rgb(summary_seg,colors);%'jet','w','shuffle');
    img_pos_or_netural = img_pos;
    img_pos_or_netural(repmat(mask_neutral & ~mask_pos,1,1,3) ) = 122;

    img_neutral = ones(size(seg,1), size(seg,2),3);
    for l = handles.labels
        imName = sprintf(handles.file_mask, l, handles.frames{frameidx});
        seg = imread(imName);
        LRGB = label2rgb(seg,colors);%'jet','w','shuffle');
        %img_pos(repmat(seg == 1,1,1,3)) = double(repmat(seg == 1,1,1,3)).*LRGB;
        img_neutral(repmat(seg == 3,1,1,3)) = LRGB(repmat(seg == 3,1,1,3));
        %img(jth*size(seg,1)+(1:size(seg,1)), ith*size(seg,2) + (1:size(seg,2)),:) = LRGB;       
    end
    img = img_pos_or_netural;%[img_pos,img_pos_or_netural, img_neutral];
    
% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.maxNumLabels
colors =  double(squeeze(label2rgb(1:(handles.maxNumLabels+1),'jet','w','shuffle')))/255;
if(handles.singleMode)
    imToLoad = get(hObject,'Value')
    imToLoad = handles.fileNames{imToLoad}
    seg = imread(imToLoad);
    LRGB = label2rgb(seg,colors);
    imshow(LRGB, 'Parent', handles.axes1)
else
    
    frameidx = get(hObject,'Value')
    img = createImage(hObject, handles, frameidx);
    %imshow([img_pos,img_pos_or_netural, img_neutral], 'Parent', handles.axes1)
    imshow(img, 'Parent', handles.axes1)
    %title('Positive labels, positive or neutral labels, neutral only')
end

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)

overall_img = [];
img = createImage(hObject, handles, 1);
img_size = size(img);
size_row_4 = [size(img,1),4*size(img,2),3];
for i = 1:length(handles.frames)
    ith = mod(i-1,4);
    jth = floor((i-1)/4);
    if mod(i,4) == 1
       img_row = ones(size_row_4); 
    
       %overall_img = [overall_img;img_row];
    end
   img = createImage(hObject, handles, i);
   %overall_img(jth*img_size(1) + (1:img_size(1)),ith*img_size(2)+ (1:img_size(2)),:) = img;
   imwrite(img, sprintf('gt_summary_frame%d.png', handles.frames{i}))
end
